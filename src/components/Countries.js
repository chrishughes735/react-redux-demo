import React from 'react';
import { requestApiData } from '../actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import DataTable from './DataTable';
import Typography from 'material-ui/Typography';

class Countries extends React.Component {
  componentDidMount() {
    if (this.props.data.length === 0) {
      this.props.requestApiData();
    }
  }
  render() {
    return (
      <div>
        <Typography variant="title" gutterBottom>
          Countries in the world
        </Typography>
        <DataTable data={this.props.data} />
      </div>
    );
  }
}

const mapStateToProps = state => ({ data: state.data });

const mapDispatchToProps = dispatch =>
  bindActionCreators({ requestApiData }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Countries);
