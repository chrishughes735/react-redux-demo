import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, {
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto'
  },
  table: {
    minWidth: 700
  }
});

function SimpleTable(props) {
  const { classes, data } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Capital</TableCell>
            <TableCell numeric>Population</TableCell>
            <TableCell numeric>Flag</TableCell>
            <TableCell numeric>Region</TableCell>
            <TableCell numeric>Area</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(n => {
            return (
              <TableRow key={n.name}>
                <TableCell>{n.name}</TableCell>
                <TableCell>{n.capital}</TableCell>
                <TableCell numeric>{n.population}</TableCell>
                <TableCell>
                  <img
                    src={n.flag}
                    alt={n.flag}
                    style={{ width: 80, height: 'auto' }}
                  />
                </TableCell>
                <TableCell numeric>{n.region}</TableCell>
                <TableCell numeric>{n.area}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

SimpleTable.propTypes = {
  classes: PropTypes.object.isRequired,
  data: PropTypes.array.isRequired
};

export default withStyles(styles)(SimpleTable);
